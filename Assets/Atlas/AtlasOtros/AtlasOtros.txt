{"frames": {

"afros_2x.png":
{
	"frame": {"x":895,"y":1092,"w":174,"h":67},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":174,"h":67},
	"sourceSize": {"w":174,"h":67}
},
"bronce_2x.png":
{
	"frame": {"x":1149,"y":207,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"btn_cerrar_2x.png":
{
	"frame": {"x":1149,"y":2,"w":111,"h":111},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":111,"h":111},
	"sourceSize": {"w":111,"h":111}
},
"campesinos_2x.png":
{
	"frame": {"x":260,"y":138,"w":289,"h":65},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":289,"h":65},
	"sourceSize": {"w":289,"h":65}
},
"creditos_2x.png":
{
	"frame": {"x":2,"y":2,"w":256,"h":793},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":0,"w":256,"h":793},
	"sourceSize": {"w":258,"h":793}
},
"flecha_atras_2x.png":
{
	"frame": {"x":618,"y":51,"w":41,"h":47},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":41,"h":47},
	"sourceSize": {"w":41,"h":47}
},
"flecha_siguiente.png":
{
	"frame": {"x":1149,"y":115,"w":78,"h":90},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":78,"h":90},
	"sourceSize": {"w":78,"h":90}
},
"flecha_siguiente_2x.png":
{
	"frame": {"x":618,"y":2,"w":41,"h":47},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":41,"h":47},
	"sourceSize": {"w":41,"h":47}
},
"logo_2x.png":
{
	"frame": {"x":664,"y":1092,"w":229,"h":150},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":229,"h":150},
	"sourceSize": {"w":229,"h":150}
},
"mizaks_2x.png":
{
	"frame": {"x":260,"y":2,"w":356,"h":67},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":356,"h":67},
	"sourceSize": {"w":356,"h":67}
},
"nasas_2x.png":
{
	"frame": {"x":895,"y":1161,"w":143,"h":67},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":143,"h":67},
	"sourceSize": {"w":143,"h":67}
},
"oro_2x.png":
{
	"frame": {"x":551,"y":166,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"plata_2x.png":
{
	"frame": {"x":566,"y":100,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"rayos_2x.png":
{
	"frame": {"x":260,"y":205,"w":242,"h":170},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":242,"h":170},
	"sourceSize": {"w":242,"h":170}
},
"texto_2x.png":
{
	"frame": {"x":260,"y":741,"w":469,"h":51},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":469,"h":51},
	"sourceSize": {"w":469,"h":51}
},
"transparencia_2x.png":
{
	"frame": {"x":504,"y":232,"w":145,"h":138},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":145,"h":138},
	"sourceSize": {"w":145,"h":138}
},
"tuto_1_2x.png":
{
	"frame": {"x":260,"y":377,"w":483,"h":362},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":483,"h":362},
	"sourceSize": {"w":483,"h":362}
},
"tuto_2_2x.png":
{
	"frame": {"x":745,"y":729,"w":482,"h":361},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":482,"h":361},
	"sourceSize": {"w":482,"h":361}
},
"tuto_3_2x.png":
{
	"frame": {"x":745,"y":366,"w":482,"h":361},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":482,"h":361},
	"sourceSize": {"w":482,"h":361}
},
"tuto_4_2x.png":
{
	"frame": {"x":664,"y":2,"w":483,"h":362},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":483,"h":362},
	"sourceSize": {"w":483,"h":362}
},
"ventana emergente_2x.png":
{
	"frame": {"x":2,"y":797,"w":660,"h":468},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":660,"h":468},
	"sourceSize": {"w":660,"h":468}
},
"yanaconas_2x.png":
{
	"frame": {"x":260,"y":71,"w":304,"h":65},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":304,"h":65},
	"sourceSize": {"w":304,"h":65}
}},
"meta": {
	"app": "http://www.codeandweb.com/texturepacker ",
	"version": "1.0",
	"image": "AtlasOtros.png",
	"format": "RGBA8888",
	"size": {"w":1267,"h":1267},
	"scale": "1",
	"smartupdate": "$TexturePacker:SmartUpdate:39fa3ea19147684bd6c8987217dd2691:f2d7e39e367902522e5a70ece527da8f:e65e6de9e80940536c2f8ba43ee730da$"
}
}
