﻿using UnityEngine;
using System.Collections;

public class PreferenceManager{
	
	enum states : int {blocked = 0, available = 1, passed = 2};

	//PreferenceManager.nextLevel(nivel + 1 , 1);
	//int nivelSiguiente = PreferenceManager.getStars(nivelActual + 1);
	
	/*
	 * Recupera las estrellas de un nivel o su
	 * estado en caso de no tener estrellas
	 * 
	 * Retorna
	 * 	-1: si el nivel está bloqueado
	 * 	0: si el nivel está habilitado
	 *  # de estrellas: si el nivel ya se superó
	 * 
	 * Parámetros
	 * 	level: nivel del cual se desea obtener información
	 */
//	public static int getStars(int level){
//
//
//		int state = PlayerPrefs.GetInt("lvl_" + level + "_state",0);
//		int result;
//		switch(state){
//		case 0:
//			if(level == 1){
//				result = 0;
//			} else{
//				result = -1;
//			}
//			break;
//		case 1:
//			result = 0;
//			break;
//		case 2:
//			result = PlayerPrefs.GetInt("lvl_" + level + "_stars");
//			break;
//		default:
//			if(level == 1){
//				result = 0;
//			} else{
//				result = -1;
//			}
//			break;
//		}
//		return result;
//	}

	#region método modifcado
	// ---------------------
	public static int getStars(string ethnicity, int level){
		
		
		int state = PlayerPrefs.GetInt("Ethnicity_" + ethnicity + "_lvl_" + level + "_state",0);
		int result;
		switch(state){
		case 0:
			if(level == 1){
				result = 0;
			} else{
				result = -1;
			}
			break;
		case 1:
			result = 0;
			break;
		case 2:
			result = PlayerPrefs.GetInt("Ethnicity_" + ethnicity + "_lvl_" + level + "_stars");
			break;
		default:
			if(level == 1){
				result = 0;
			} else{
				result = -1;
			}
			break;
		}
		return result;
	}
	// ---------------------
	#endregion
	
	/*
	 * Actualiza los registro cuando se avanza de nivel
	 * 
	 * Parámetros
	 * 	level: nivel que se acaba de superar
	 * 	stars: estrellas conseguidas en ese nivel
	 */ 
//	public static void nextLevel(int level, int stars){
//		int previousStars = PlayerPrefs.GetInt("lvl_" + level + "_stars");
//		if(previousStars < stars){
//			PlayerPrefs.SetInt("lvl_" + level + "_stars", stars);
//			PlayerPrefs.SetInt("lvl_" + level + "_state",(int)states.passed);
//			if(getStars(level + 1) < 2){
//				PlayerPrefs.SetInt("lvl_" + (level+1) + "_state",(int)states.available);	
//			}
//		}
//	}

	#region método modificado
	// --------------------
	public static bool nextLevel(string ethnicity, int level, int stars){
		int previousStars = PlayerPrefs.GetInt("Ethnicity_" + ethnicity + "_lvl_" + level + "_stars");
		bool beatScore = false;
		if(previousStars < stars){
			beatScore = true;
			PlayerPrefs.SetInt("Ethnicity_" + ethnicity + "_lvl_" + level + "_stars", stars);
			PlayerPrefs.SetInt("Ethnicity_" + ethnicity + "_lvl_" + level + "_state",(int)states.passed);
			if(getStars(ethnicity, level + 1) < 2){
				PlayerPrefs.SetInt("Ethnicity_" + ethnicity + "_lvl_" + (level+1) + "_state",(int)states.available);	
			}
		}

		return beatScore;
	}
	// --------------------
	#endregion
	 
	
}
