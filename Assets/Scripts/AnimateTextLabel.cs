﻿using UnityEngine;
using System.Collections;

public class AnimateTextLabel : MonoBehaviour {

	public int indexText = 0;
	public string [] textToLoad;

	// Use this for initialization
	void Start ()
	{
		StartCoroutine("Wait");
	}

	void AnimateText()
	{

		indexText++;
		
		if(indexText > 2)
		{
			indexText = 0;
		}

		this.GetComponent<UILabel>().text = textToLoad[indexText];

	}

	IEnumerator Wait()
	{
		yield return new WaitForFixedUpdate();
		InvokeRepeating("AnimateText", 1.0f, 1.0f);	
	}
}
