﻿using UnityEngine;
using System.Collections;

public class VefificateSound : MonoBehaviour {

	private string valueMute;

	// Use this for initialization
	void Update ()
	{
		valueMute = PlayerPrefs.GetString("Mute");

		if(this.gameObject.GetComponent<AudioSource>() != null)
		{
			switch (valueMute)
			{
			case "true":
				audio.mute = true;
				break;

			case "false":
				audio.mute = false;
				break;
			default:
			break;
			}
		}
	}
}
