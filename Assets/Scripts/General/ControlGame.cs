﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class ControlGame : MonoBehaviour
{
	// ---------- UI2DSprite
	#region ---------- UI2DSprite
	[SerializeField]
	public static UI2DSprite sprite2D;
	#endregion
	

	public BoxCollider colliderCube;
	public UISprite imageLvl;
	public UIButton [] buttons;
	public GameObject puzzleToResolved;
	public Texture [] backgrounds;
	public int indiceFondos = 0;
	public GameObject fondo;

	private TestJigsawPuzzle scriptJigsaw;

	public TweenPosition containerScrollView;
	public TweenPosition containerPanelPaused;
	public TweenPosition containerSeeImage;

	public GameObject bck;

	public UIButton [] closeButtons;

	public UIButton menuButton;
	public UIButton restartButton;

	//public UISprite imageToShow;
	// TODO Se reemplazo esta línea de código
	public UITexture imageToShow;

	public GameObject puzzleText;
	public UILabel labelPuzzleImageText;


	// TODO Reemplace estas lineas de codigo
//	public UISprite ImageFinishedPuzzle;
//	public UISprite ImageInfoPuzzle;

	public UITexture ImageFinishedPuzzle;
	public UITexture ImageInfoPuzzle;


	public UITexture flash;
	public UITexture ImagePuzzleHelp;

	public UILabel labelTitle;

	private bool clipPlayOneTime = false;
	private bool clipPlayOneTimeShowInfo = false;

	#region Arreglos Textos Étnias
	public ArregloMultidimensional [] textosCampesinos = new ArregloMultidimensional[1];
	public ArregloMultidimensional [] textosAfro = new ArregloMultidimensional[1];
	public ArregloMultidimensional [] textosGuambianos = new ArregloMultidimensional[1];
	public ArregloMultidimensional [] textosNasa = new ArregloMultidimensional[1];
	public ArregloMultidimensional [] textosYanaconas = new ArregloMultidimensional[1];

	public ArregloMultidimensional [] textosTitulos = new ArregloMultidimensional[1];


	#endregion 

	public GameObject buttonConfig;

	public AudioClip clipShowImage;
	public AudioClip clipPuzzleCompleted;

	// Atlas Actual 
	#region Atlas Actual
	public UIAtlas atlasActual;
	public UIAtlas [] arrayAtlas;
	#endregion

	public List<Sprite> sprites;

	void Awake()
	{
//		sprite2D = transform.Find("2DSprite").gameObject.GetComponent<UI2DSprite>();
//		sprites = Resources.LoadAll<Sprite>(@"TextureAfro/Afro").ToList();
//
//		string nameComplete;
//		string [] partsOfName;
//		
//		nameComplete = PlayerPrefs.GetString("ImagenRompecabezas");
//		partsOfName = nameComplete.Split(new char[]{ '_' });
//
//
//		sprite2D.sprite2D = sprites[int.Parse(partsOfName[1])];

		Debug.Log(PlayerPrefs.GetString("ImagenRompecabezas"));

		switch (PlayerPrefs.GetString("Ethnicity"))
		{
		case "Afro":
			atlasActual = arrayAtlas[0];
			Debug.Log("Atlas Actual: Afros");
			break;
			
		case "Campesinos":
			atlasActual = arrayAtlas[1];
			Debug.Log("Atlas Actual: Campesinos");
			break;
			
		case "Guambianos":
			atlasActual = arrayAtlas[2];
			Debug.Log("Atlas Actual: Guambianos");
			break;
			
		case "Nasa":
			atlasActual = arrayAtlas[3];
			Debug.Log("Atlas Actual: Nasa");
			break;
			
		case "Yanaconas":
			atlasActual = arrayAtlas[4];
			Debug.Log("Atlas Actual: Yanaconas");
			break;
		default:
			break;
		}


	}

	// Use this for initialization
	void Start ()
	{
		// Set Atlas
//		#region Set Atlas
//		imageToShow.atlas = atlasActual;
//		ImageFinishedPuzzle.atlas = atlasActual;
//		ImageInfoPuzzle.atlas = atlasActual;
//		#endregion


		clipPlayOneTime = false;
		clipPlayOneTimeShowInfo = false;
		imageLvl.spriteName = PlayerPrefs.GetString("ImagenRompecabezas");
		buttonConfig.SendMessage("OnClick");

		puzzleToResolved = GameObject.Find("Puzzle To Resolved");

		puzzleText = GameObject.Find("UI Root/Camera/HUD/Puzzle Controller/PanelStoryImage/ContainerScroll/ScrollViewContent/LabelContent");
		labelPuzzleImageText = puzzleText.GetComponent<UILabel>();

		fondo = puzzleToResolved.transform.FindChild("Background").gameObject;


		// Reproducir sonido de mostrar ventana de información
		EventDelegate.Add(containerScrollView.onFinished, delegate(){
			if(PlayerPrefs.GetString("Mute") == "false" && !clipPlayOneTimeShowInfo)
			{
				clipPlayOneTimeShowInfo = true;
				audio.PlayOneShot(clipShowImage);
			}
		});


		scriptJigsaw = puzzleToResolved.transform.FindChild("Cube").gameObject.GetComponent<TestJigsawPuzzle>();
		colliderCube = puzzleToResolved.transform.FindChild("Collider").GetComponent<BoxCollider>();




		EventDelegate.Add(buttons[0].onClick, delegate ()
		                  {

			indiceFondos++;

			if(indiceFondos > 3)
			{
				indiceFondos = 0;
			}

			fondo.GetComponent<MeshRenderer>().material.mainTexture = backgrounds[indiceFondos];

		});

		EventDelegate.Add(buttons[1].onClick, delegate ()
		                  {

			bck.GetComponent<BoxCollider>().isTrigger = false;
			bck.GetComponent<TweenAlpha>().PlayForward();
			Time.timeScale = 0;
			containerPanelPaused.PlayForward();		
		});

		#region Close Panel Paused
//		EventDelegate.Add(closeButtons[0].onClick, delegate ()
//		                  {
//			
//			bck.GetComponent<BoxCollider>().isTrigger = true;
//			bck.GetComponent<TweenAlpha>().PlayReverse();
//			Time.timeScale = 1;
//			containerPanelPaused.PlayReverse();		
//		});

		EventDelegate.Add(closeButtons[1].onClick, delegate ()
		                  {
			
			bck.GetComponent<BoxCollider>().isTrigger = true;
			bck.GetComponent<TweenAlpha>().PlayReverse();
			Time.timeScale = 1;
			containerPanelPaused.PlayReverse();		
		});
		#endregion

		EventDelegate.Add(buttons[3].onClick, delegate ()
		                  { 
//			#region -> Eliminar Puzzle
//			if (Hud.hudPUP_Parent != null) {
//				
//				// Desactivamos la pantalla actual
//				Hud.hudPUP_Parent.SetActive (false); // (opcional)
//				
//				// Eliminamos HUD padre
//				GameObject.Destroy (Hud.hudPUP_Parent);
//				Hud.hudPUP_Parent = null;
//			}
//			#endregion

			PlayerPrefs.GetInt("Solved", 0); });
			//PlayerPrefs.SetString("Launch", "true");

				





		// Ir a seleccion de niveles (de la misma étnia)
		NavigationMap.ChangeOfScreen(buttons[3].gameObject, eScreen.LEVELS);


		// Ir a la pantalla de logo
		NavigationMap.ChangeOfScreen(menuButton.gameObject, eScreen.LOGO);

		//			#region -> Eliminar Puzzle
		//			if (hudPUP_Parent != null) {
		//				
		//				// Desactivamos la pantalla actual
		//				hudPUP_Parent.SetActive (false); // (opcional)
		//				
		//				// Eliminamos HUD padre
		//				GameObject.Destroy (hudPUP_Parent);
		//				hudPUP_Parent = null;
		//			}
		//			#endregion

		EventDelegate.Add(menuButton.onClick, delegate ()
		                  { 

//			#region -> Eliminar Puzzle
//			if (Hud.hudPUP_Parent != null) {
//				
//				// Desactivamos la pantalla actual
//				Hud.hudPUP_Parent.SetActive (false); // (opcional)
//				
//				// Eliminamos HUD padre
//				GameObject.Destroy (Hud.hudPUP_Parent);
//				Hud.hudPUP_Parent = null;
//			}
//			#endregion

			PlayerPrefs.SetInt("Solved", 0);
			//PlayerPrefs.SetString("Launch", "true");
			


			 });

		#region Reiniciar el rompecabezas
		EventDelegate.Add(restartButton.onClick, delegate ()
		                  {

			bck.GetComponent<BoxCollider>().isTrigger = true;
			bck.GetComponent<TweenAlpha>().PlayReverse();

			scriptJigsaw.Restart();

			containerPanelPaused.PlayReverse();
			if(containerPanelPaused.direction == AnimationOrTween.Direction.Reverse)
			{
				EventDelegate.Add(containerPanelPaused.onFinished, delegate() {  });
			}
		});
		#endregion


		EventDelegate.Add(buttons[2].onClick, delegate ()
		                  {

			// TODO Reemplazar esta línea de código

			//imageToShow.spriteName = PlayerPrefs.GetString("ImagenRompecabezas") + ".jpg";
			imageToShow.mainTexture = (Texture)Resources.Load("Images/" + PlayerPrefs.GetString("ImagenRompecabezas"), typeof(Texture));

			bck.GetComponent<BoxCollider>().isTrigger = false;
			bck.GetComponent<TweenAlpha>().PlayForward();
			Time.timeScale = 0;
			containerSeeImage.PlayForward();		
		});

		EventDelegate.Add(closeButtons[2].onClick, delegate ()
		                  {
			
			bck.GetComponent<BoxCollider>().isTrigger = true;
			bck.GetComponent<TweenAlpha>().PlayReverse();
			Time.timeScale = 1;
			containerSeeImage.PlayReverse();		
		});



			// TODO Reemplace estas lineas de código
			//ImageFinishedPuzzle.spriteName = PlayerPrefs.GetString("ImagenRompecabezas") + ".jpg";
			ImageFinishedPuzzle.mainTexture = (Texture)Resources.Load("Images/" + PlayerPrefs.GetString("ImagenRompecabezas"), typeof(Texture));

//			ImageInfoPuzzle.spriteName = PlayerPrefs.GetString("ImagenRompecabezas") + ".jpg";
				ImageInfoPuzzle.mainTexture = (Texture)Resources.Load("Images/" + PlayerPrefs.GetString("ImagenRompecabezas"), typeof(Texture));
	
	}

	void ChangeBackground(GameObject gO)
	{
		Debug.LogError("Nombre de este boton: " + gO);
	}



	void PassLevel()
	{
		string nameComplete;
		string [] partsOfName;
		
		nameComplete = PlayerPrefs.GetString("NameLevel");
		partsOfName = nameComplete.Split(new char[]{ '_' });



		switch (PlayerPrefs.GetString("Difficulty"))
		{
			case "facil":
			PreferenceManager.nextLevel(PlayerPrefs.GetString("Ethnicity") ,int.Parse(partsOfName[1]), 1);
				break;
			case "medio":
			PreferenceManager.nextLevel(PlayerPrefs.GetString("Ethnicity"), int.Parse(partsOfName[1]), 2);
				break;
			case "dificil":
			PreferenceManager.nextLevel(PlayerPrefs.GetString("Ethnicity"), int.Parse(partsOfName[1]), 3);
				break;
				default:
						break;
		}




		#region Texts
		string nameCompleteRegion;
		string [] partsOfNameRegion;
		nameCompleteRegion = PlayerPrefs.GetString("ImagenRompecabezas");
		partsOfNameRegion = nameCompleteRegion.Split(new char[]{ '_' });
		
		switch (partsOfNameRegion[0])
		{
		case "Afro":
			labelPuzzleImageText.text = textosAfro[0][int.Parse(partsOfName[1])];
			labelTitle.text = textosTitulos[0][(int.Parse(partsOfName[1]) - 1)];
			break;
			
		case "Campesinos":
			labelPuzzleImageText.text = textosCampesinos[0][int.Parse(partsOfName[1])];
			labelTitle.text = textosTitulos[1][(int.Parse(partsOfName[1]) - 1)];
				break;
			
		case "Guambianos":
			labelPuzzleImageText.text = textosGuambianos[0][int.Parse(partsOfName[1])];
			labelTitle.text = textosTitulos[2][(int.Parse(partsOfName[1]) - 1)];
				break;
			
		case "Nasa":
			labelPuzzleImageText.text = textosNasa[0][int.Parse(partsOfName[1])];
			labelTitle.text = textosTitulos[3][(int.Parse(partsOfName[1]) - 1)];
				break;
			
		case "Yanaconas":
			labelPuzzleImageText.text = textosYanaconas[0][int.Parse(partsOfName[1])];
			labelTitle.text = textosTitulos[4][(int.Parse(partsOfName[1]) - 1)];
				break;
			
		default:
			break;
		}


		
		#endregion

		StateLvl.BeatScoreFlag("Si", PlayerPrefs.GetString("NameLevel"));


	}

	public static bool movePieces;

	 
	// Update is called once per frame
	void Update ()
	{
		if(Input.GetKeyDown(KeyCode.Y))
		{
		}

		if(!bck.GetComponent<BoxCollider>().isTrigger)
			movePieces = false;
		else
			movePieces = true;

		if(scriptJigsaw.solved)		
		{
			if(PlayerPrefs.GetString("Mute") == "false" && !clipPlayOneTime)
			{
				clipPlayOneTime = true;
				AudioSource.PlayClipAtPoint(clipPuzzleCompleted, this.transform.position);
			}

			bck.GetComponent<BoxCollider>().isTrigger = false;
			bck.GetComponent<TweenAlpha>().PlayForward();
			Time.timeScale = 0;

			ImageFinishedPuzzle.GetComponent<TweenColor>().PlayForward();


			EventDelegate.Add(ImageFinishedPuzzle.GetComponent<TweenColor>().onFinished, delegate() { 

				if(ImageFinishedPuzzle.GetComponent<TweenColor>().direction == AnimationOrTween.Direction.Forward)
				{
					TweenColor tweenColorImage = TweenColor.Begin(ImageFinishedPuzzle.gameObject, 1.0f, new Color(ImageFinishedPuzzle.color.r, ImageFinishedPuzzle.color.g, ImageFinishedPuzzle.color.b, 0.0f));
					tweenColorImage.delay = 2.0f;

				}
			});


			EventDelegate.Add(flash.GetComponent<TweenColor>().onFinished, delegate() { 
				
				if(flash.GetComponent<TweenColor>().direction == AnimationOrTween.Direction.Forward)
				{
					TweenColor tweenColorFlash = TweenColor.Begin(flash.gameObject, 1.0f, new Color(flash.color.r, flash.color.g, flash.color.b, 0.0f));
					tweenColorFlash.duration = 0.8f;
					tweenColorFlash.delay = 0.5f; 



				}
			});

			containerScrollView.PlayForward();



			PassLevel();



		}
	
	}
}
