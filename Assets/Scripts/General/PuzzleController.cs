﻿using UnityEngine;
using System.Collections;

public class PuzzleController : JigsawPuzzle {

	private float _LightUp;
	private float _Color;
	
	// is true when the puzzle is solved
	public bool solved
	{
		get
		{
			return _solved;
		}
	}
	
	// contains the move count if the puzzle is solved
	public int moves
	{
		get
		{
			return _moves;
		}
	}
	
	// contains the puzzle time if the puzzle is solved
	public float time
	{
		get
		{
			return _time;
		}
	}
	
	bool _solved = false;
	int _moves = 0;
	float _time = 0.0f;
	
	// Use this for initialization
	void Start () {	
	}
	
	// Update is called once per frame
	new void Update () {
		// call inherited JigsawPuzzle.Update();
		base.Update();
		
		if(Input.GetKeyDown(KeyCode.F))
		{
			base.Restart();
		}
	}
	
	// PuzzleStart is called when a new puzzle is started
	protected override void PuzzleStart()
	{
		_solved = false;
	}
	
	// ActivatePiece is called when one clicks - and hold mouse left button
	// on a (loose) puzzle piece.
	protected override void ActivatePiece(GameObject piece)
	{
		// show real colors of this piece by removing brightness and 
		// adding all color with the 'shaderGrayColored' shader
		_LightUp = piece.renderer.material.GetFloat("_LightUp");
		_Color = piece.renderer.material.GetFloat("_Color");
		piece.renderer.material.SetFloat("_LightUp", 0.01f);
		piece.renderer.material.SetFloat("_Color", 1);
	}
	
	// DeativatePiece is called when one releases left mouse button
	// and a puzzle piece was active
	protected override void DeactivatePiece(GameObject piece)
	{
		// reset LightUp and Color of the shader
		piece.renderer.material.SetFloat("_LightUp", _LightUp);
		piece.renderer.material.SetFloat("_Color", _Color);
	}
	
	protected override void PiecePlaced(GameObject piece)
	{
		// show real colors of this piece by removing Lightness and 
		// adding all color with the 'shaderGrayColored' shader
		piece.renderer.material.SetFloat("_LightUp", 0.01f);
		piece.renderer.material.SetFloat("_Color", 1f);
	}
	
	// PiecePlace is called when a puzzle piece is fit on the correct spot 
	protected override void PuzzleSolved(int moves, float time)
	{
		_solved = true;
		_moves = moves;
		_time = time;
	}
	
	// Scatter Piece is called to set the 'scattered' place of a puzzle piece when starting a puzzle
	// return null to perform 'default' scattering. The starting location of a piece is it's right
	// puzzle spot.
	protected override GameObject ScatterPiece(GameObject piece)
	{
		return null;
	}

}
