﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LvlsController : MonoBehaviour {

	public int level;
	public int stars;

	public bool updateLvl;

	public GameObject levels;
	public GameObject lines;

	public string numeros;

	[SerializeField]
	private List <GameObject> levelsButtons;

	[SerializeField]
	private List <GameObject> levelsLines;

	[SerializeField]
	private List <GameObject> levelsPassed;

	[SerializeField]
	private List <string> indixesLevels;

	public int nivelesPasados = 0;
	GameObject gO;

	public int numero;
	[SerializeField]
	private List <int> numerosObtenidos;

	[SerializeField]
	private List <GameObject> lineasAObtener;
	public GameObject linesParent;

	private bool unaVezLineas = false;

	float valueLerp;

	public TweenPosition allTweens;

	public UISprite spriteTitle;
	private int numberOfLevels;

	void SetTitleWorld()
	{




		switch (PlayerPrefs.GetString("Ethnicity"))
		{
		case "Afro":
			spriteTitle.spriteName = "afros_2x";
			numberOfLevels = 10;

			levels.transform.Find("L_10").GetComponent<UIButton>().isEnabled = true;

			break;
			
		case "Campesinos":
			spriteTitle.spriteName = "campesinos_2x";
			numberOfLevels = 9;
			levels.transform.Find("L_10").GetComponent<UIButton>().isEnabled = false;

			break;
			
		case "Guambianos":
			spriteTitle.spriteName = "mizaks_2x";
			numberOfLevels = 9;
			levels.transform.Find("L_10").GetComponent<UIButton>().isEnabled = false;

			break;
			
		case "Nasa":
			spriteTitle.spriteName = "nasas_2x";
			numberOfLevels = 9;
			levels.transform.Find("L_10").GetComponent<UIButton>().isEnabled = false;


			break;
			
		case "Yanaconas":
			spriteTitle.spriteName = "yanaconas_2x";
			levels.transform.Find("L_10").GetComponent<UIButton>().isEnabled = false;
			numberOfLevels = 9;

			break;
			
		default:
			break;
		}

		spriteTitle.MakePixelPerfect();
		

	}
	// Use this for initialization
	void Start ()
	{
		spriteTitle.gameObject.transform.localScale = new Vector3(0.85f, 0.85f, 1f);

		foreach (Transform levelCurrent in levels.transform)
		{
			levelsButtons.Add(levelCurrent.gameObject);

		}

		foreach (Transform lineCurrent in lines.transform)
		{
			levelsLines.Add(lineCurrent.gameObject);
		}

		for (int i = 0; i < levelsButtons.Count; i++)
		{
			SetTextureLevel(levelsButtons[i]);
		}
	
		//levelsButtons.Sort(new CustomGameObjectSortComparer(levelsButtons));
		IComparer c = new CaseInsensitiveComparer();

		for(int i = 0 ; i < levelsButtons.Count; i ++){
			for(int j = i+1 ; j < levelsButtons.Count; j++){
				if(c.Compare(levelsButtons[j].name , levelsButtons[i].name) == -1){
					GameObject temp = levelsButtons[i];
					levelsButtons[i] = levelsButtons[j];
					levelsButtons[j] = temp;
				}
			}
		}

		//levelsLines.Sort();

		StartCoroutine(CheckLevelsState());


		for (int t = 0; t < levelsButtons.Count; t++)
		{
			NavigationMap.ChangeOfScreen(levelsButtons[t], eScreen.SETTING_LEVEL);
		}


		foreach (GameObject buttonOfList in levelsButtons)
		{
			UIEventListener.Get(buttonOfList).onClick += delegate (GameObject gO)
			{
//				Debug.Log("Oprimio Boton " + gO.name);
//				GameObject sprite = buttonOfList.transform.FindChild("Background").gameObject;
				PlayerPrefs.SetString("NameLevel", gO.name);
				PlayerPrefs.SetString("ImagenRompecabezas", gO.transform.FindChild("Background").gameObject.GetComponent<UISprite>().spriteName);
//				Debug.LogError("Nombre de la imagen: " + PlayerPrefs.GetString("ImagenRompecabezas"));
			};


			
		}

		SetTitleWorld();
		#region fillAmount = 0


//		for (int v = 0; v < levelsButtons.Count; v++)
//		{
//			if(/*levelsButtons[v].GetComponent<StateLvl>().currentState == States.AVAILABLE||*/ levelsButtons[v].GetComponent<StateLvl>().currentState == States.PASSED)
//			{
//				levelsButtons[v].transform.FindChild("L").GetComponent<UISprite>().fillAmount = 1.0f;	
//			}
//			
//			
//			
//			
//		}
		#endregion


		
		}

	void DefinirImagen(GameObject objeto)
	{
		Debug.Log(objeto.name);
	}

	IEnumerator CheckLevelsState()
	{
		levelsPassed.Sort();
		yield return new WaitForEndOfFrame();
		for (int d = 0; d < levelsButtons.Count; d++)
		{


			if(levelsButtons[d].GetComponent<StateLvl>().currentState == States.PASSED || levelsButtons[d].GetComponent<StateLvl>().currentState == States.AVAILABLE)
			{
//				Debug.Log("Disponibles "  + " Nombre: " + levelsButtons[d].name);
				levelsPassed.Add(levelsButtons[d]);
			}
			else if(levelsButtons[d].GetComponent<StateLvl>().currentState == States.BLOCKED)
				levelsPassed.Remove(levelsButtons[d]);



			yield return new WaitForEndOfFrame();
			//setLine(levelsPassed[d], levelsLines[d]);

		}




//		for (int g = 0; g < levelsPassed.Count; g++)
//		{
//			string nameComplete;
//			string [] partsOfName;
//			
//			nameComplete = levelsPassed[g].name;
//			partsOfName = nameComplete.Split(new char[]{ '_' });
//			numerosObtenidos.Add(int.Parse(partsOfName[1]));
//
//			for (int r = 0; r < numerosObtenidos.Count - 1; r++)
//			{
//				yield return new WaitForEndOfFrame();
//				Debug.Log("Estos son las lineas obtenidas: " + levelsLines[r].name);
//				SetTweenFill(levelsLines[r].gameObject);
//			}
//		}

//		linesParent = GameObject.Find("UI Root/Camera/HUD/Levels/Lines");
//
//		for (int g = 0; g < levelsPassed.Count; g++)
//		{
//			string nameComplete;
//			string [] partsOfName;
//			
//			nameComplete = levelsPassed[g].name;
//			partsOfName = nameComplete.Split(new char[]{ '_' });
//			numerosObtenidos.Add(int.Parse(partsOfName[1]));
//			
//			for (int r = 0; r < numerosObtenidos.Count; r++)
//			{
//				
//				//Debug.Log("Estos son las lineas obtenidas: " + levelsLines[r].name);
////				SetTweenFill(levelsLines[r].gameObject);
//				if(levelsLines[r].name == "L_" + numerosObtenidos[r])
//				{
//					lineasAObtener.Add(levelsLines[r]);
//				}
//				
//				
//			}
//		}


	}


	void setLine(GameObject lineReceive)
	{
		string nameComplete;
		string [] partsOfName;
		
		nameComplete = lineReceive.name;
		partsOfName = nameComplete.Split(new char[]{ '_' });

//		Debug.Log(partsOfName[1]);
//
//		Debug.Log ("L_" + partsOfName[1]);


//		if(levelsLines.Contains(lineReceive))
//		{
//			Debug.Log("Objecto resivido: " + lineReceive);
//			//SetTweenFill(levelsLines[r].gameObject);
//		}

		for (int r = 0; r < levelsLines.Count; r++)
		{
			levelsLines[r].GetComponent<TweenScale>().Play();
			SetTweenFill(levelsLines[r].gameObject);

//			if(levelsLines[r].name == "L_" + partsOfName[1])
//			{
//				Debug.Log("Se cumple");
////				levelsLines[r].GetComponent<TweenScale>().Play();
//				SetTweenFill(levelsLines[r].gameObject);
//
//
//
//			}
		}
	}

	void SetTweenFill(GameObject gO)
	{
				
		
		TweenFill tweenFill = TweenFill.Begin(gO, 1.0f, 1.0f);
		tweenFill.from = 0.0f;
		tweenFill.to = 1.0f;
	}

	void SetTextureLevel(GameObject objectReceive)
	{
		string nameComplete;
		string [] partsOfName;
		
		nameComplete = objectReceive.name;
		partsOfName = nameComplete.Split(new char[]{ '_' });


		// ------

		objectReceive.transform.FindChild("Background").GetComponent<UISprite>().spriteName = PlayerPrefs.GetString("Ethnicity") + "_" + partsOfName[1];
		objectReceive.GetComponent<UIButton>().normalSprite = PlayerPrefs.GetString("Ethnicity") + "_" + partsOfName[1];

//
//			for (int k = 0; k < levelsButtons.Count; k++)
//					{
//						
//						if(levelsButtons[k].GetComponent<StateLvl>().currentState == States.PASSED)
//						{
//							EventDelegate.Add(allTweens.onFinished, delegate()
//							                  {
//								if(allTweens.playDirection == AnimationOrTween.Direction.Forward)
//								{
//									levelsButtons[k].transform.FindChild("L").GetComponent<UISprite>().fillAmount = 1.0f;
//								}
//							});
//			
////							if(allTweens.direction == AnimationOrTween.Direction.Reverse)
////							{
////								levelsButtons[k].transform.FindChild("L").GetComponent<UISprite>().fillAmount = 0.0f;
////							}
//						}
//						
//						
//					}
			
	}

	void TweenFillLines(int index)
	{
		levelsPassed[index].transform.FindChild("L").GetComponent<UISprite>().fillAmount = 1.0f;
	}
	
	// Update is called once per frame
	void Update ()
	{


//		for (int k = 0; k < levelsButtons.Count; k++)
//		{
//			
//			if(levelsButtons[k].GetComponent<StateLvl>().currentState == States.AVAILABLE || levelsButtons[k].GetComponent<StateLvl>().currentState == States.PASSED)
//			{
//				EventDelegate.Add(allTweens.onFinished, delegate()
//				                  {
//					if(allTweens.direction == AnimationOrTween.Direction.Forward)
//					{
//						levelsButtons[k].transform.FindChild("L").GetComponent<UISprite>().fillAmount = 1.0f;
//					}
//				});
//
//				if(allTweens.direction == AnimationOrTween.Direction.Reverse)
//				{
//					levelsButtons[k].transform.FindChild("L").GetComponent<UISprite>().fillAmount = 0.0f;
//				}
//			}
//			
//			
//		}
		if(levelsPassed.Count <= numberOfLevels)
		{

			for (int v = 0; v < levelsPassed.Count; v++)
			{
				if(/*levelsButtons[v].GetComponent<StateLvl>().currentState == States.AVAILABLE||*/ levelsPassed[v].GetComponent<StateLvl>().currentState == States.PASSED)
				{
	//				if(allTweens.playDirection == AnimationOrTween.Direction.Forward)
	//				{


						levelsPassed[v].transform.FindChild("L").GetComponent<UISprite>().fillAmount = 1.0f;
	//				EventDelegate.Add(allTweens.onFinished, delegate()
	//				                  				                  {
	//
	//					Debug.Log("Termino el tween");
	//  					
	//						Debug.Log("La direccion del tween era forward");
	//						levelsPassed[v].transform.FindChild("L").GetComponent<UISprite>().fillAmount = 1.0f;
	//  					
	//					});
					//}
	//				allTweens = levelsButtons[0].GetComponent<UIPlayTween>();
	//				
	//				
	//				EventDelegate.Add(allTweens.onFinished, delegate()
	//				                  {
	//					levelsButtons[v].transform.FindChild("L").GetComponent<UISprite>().fillAmount = 1.0f;
	//				});
					
					
					
				}
			}
		}

		if(updateLvl)
		{
			//PreferenceManager.nextLevel(level , stars);

		}
	
		}
	}

