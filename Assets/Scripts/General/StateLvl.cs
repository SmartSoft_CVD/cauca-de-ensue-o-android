﻿using UnityEngine;
using System.Collections;

public enum States { BLOCKED, AVAILABLE, PASSED }
public class StateLvl : MonoBehaviour
{
	public Transform childObtained;
	public States currentState;

	private int state;

	public int State {
		get {
			return state;
		}
		set {
			state = value;
		}
	}

	private GameObject starsObtained;

	void Start()
	{

		string nameComplete;
		string [] partsOfName;
		
		nameComplete = this.gameObject.name;
		partsOfName = nameComplete.Split(new char[]{ '_' });


		string nameEthnicity = PlayerPrefs.GetString("Ethnicity");

		switch (nameEthnicity)
		{
			case "Afro":
			state = PreferenceManager.getStars("Afro", int.Parse(partsOfName[1]));
				break;
			case "Campesinos":
			state = PreferenceManager.getStars("Campesinos", int.Parse(partsOfName[1]));
				break;
			case "Guambianos":
			state = PreferenceManager.getStars("Guambianos", int.Parse(partsOfName[1]));
				break;
			case "Nasa":
			state = PreferenceManager.getStars("Nasa", int.Parse(partsOfName[1]));
				break;
			case "Yanaconas":
			state = PreferenceManager.getStars("Yanaconas", int.Parse(partsOfName[1]));
				break;

				default:
						break;
		}

		int previousStars = PreferenceManager.getStars(nameEthnicity, int.Parse(partsOfName[1]));


		if(state == -1)
		{
			this.gameObject.GetComponent<UIButton>().isEnabled = false;
			currentState = States.BLOCKED;
		}
		else if(state == 0)
		{
			//Here code for go to game puzzle
			currentState = States.AVAILABLE;
		}
		else
		{
			currentState = States.PASSED;

			if (starsObtained != null) {
				
				// Desactivamos la pantalla actual
				starsObtained.SetActive (false); // (opcional)
				
				// Eliminamos HUD padre
				GameObject.Destroy (starsObtained);
				starsObtained = null;
			}



			switch (state)
			{
				case 1:

					starsObtained = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Stars/OneStar"));					
					// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
					starsObtained.name = "OneStar";					
					// Lo agregamos como hijo del HUD
					starsObtained.transform.parent = this.transform;					
					// La asignaciom modifica el Transform, asi que lo reiniciamos
					starsObtained.transform.localPosition = new Vector3(-86.5f, 84.3f, 0);
					starsObtained.transform.localRotation = Quaternion.identity;
					starsObtained.transform.localScale = Vector3.one;

					break;
					
				case 2:

					starsObtained = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Stars/TwoStars"));					
					// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
					starsObtained.name = "TwoStars";					
					// Lo agregamos como hijo del HUD
					starsObtained.transform.parent = this.transform;					
					// La asignaciom modifica el Transform, asi que lo reiniciamos
					starsObtained.transform.localPosition = new Vector3(-86.5f, 84.3f, 0);
					starsObtained.transform.localRotation = Quaternion.identity;
					starsObtained.transform.localScale = Vector3.one;

					break;
					
				case 3:

					starsObtained = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Stars/ThreeStars"));					
					// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
					starsObtained.name = "ThreeStars";					
					// Lo agregamos como hijo del HUD
					starsObtained.transform.parent = this.transform;					
					// La asignaciom modifica el Transform, asi que lo reiniciamos
					starsObtained.transform.localPosition = new Vector3(-86.5f, 84.3f, 0);
					starsObtained.transform.localRotation = Quaternion.identity;
					starsObtained.transform.localScale = Vector3.one;

					break;

				default:
					break;
			}


			if(PlayerPrefs.GetString("Flag") == "Si")
			{
				//BeatScore(PlayerPrefs.GetString("NameLevel"), starsObtained);
				PlayerPrefs.SetString("Flag", "No");
			}


		}

	}
	public static void BeatScoreFlag(string beat, string lvl)
	{
		PlayerPrefs.SetString("Flag", beat);
		PlayerPrefs.SetString("LvlStars", lvl);

	}

	public void BeatScore(string lvl, GameObject stars)
	{
		Debug.Log("Ent(ro en la condicion");



		GameObject lvlPassed = GameObject.Find(lvl).gameObject;
		childObtained = lvlPassed.transform.GetChild(1);

		Debug.LogError("Nombre del nivel pasado: "  + lvlPassed);

		Debug.LogError("Nombre del hijo del nivel pasado: "  + childObtained);


		TweenScale tweenPosStars = TweenScale.Begin( childObtained.gameObject, 5.0f, new Vector3(2f, 2f, 2f));

		if(tweenPosStars.direction == AnimationOrTween.Direction.Forward)
		{
			EventDelegate.Add(tweenPosStars.onFinished, delegate() { Debug.Log("termino la condicion"); tweenPosStars.SetEndToCurrentValue(); });
		}

	}
}
