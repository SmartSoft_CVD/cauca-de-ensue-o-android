using UnityEngine;

public class UIScaleOnCenter : MonoBehaviour {
    
    /// <summary>
    /// Scale applies to all involved childs, dependent on distance from center object,
 	/// the higher distance the lower scale modifier is applied.
    /// </summary>
    
    public Vector3 scaleIncreaseBy = Vector3.zero;
    
    /// <summary>
    /// Variable describe how much items around centered object will take effect of scale.
    /// </summary>
    
    public int childsInvolved = 2;
    private float maxOffset;
    
    private UISprite [] items;
    private Vector3 [] startingScale;
    
    private bool scrollViewIsHorizontal;
    
	UIScrollView mDrag;
    
    void Start() {
        if(mDrag == null) {
			mDrag = NGUITools.FindInParents<UIScrollView>(gameObject);
            
            if(mDrag == null) {
				Debug.LogWarning(GetType() + " requires " + typeof(UIScrollView) + " on a parent object in order to work", this);
                Destroy(this);
            }
            
        }
        
        UIGrid grid = GetComponent<UIGrid>();
        
        if(grid == null) {
            Debug.LogWarning(GetType() + " requires " + typeof(UIGrid) + " object in order to work", this);
            Destroy(this);
        }
        
        if(grid.arrangement == UIGrid.Arrangement.Horizontal) {
            maxOffset = grid.cellWidth * childsInvolved;
            scrollViewIsHorizontal = true;
        } else {
            scrollViewIsHorizontal = false;
            maxOffset = grid.cellHeight * childsInvolved;
        }
        
        Transform trans = transform;
        int childCount = trans.childCount;
        
        items = new UISprite[childCount];
        startingScale = new Vector3[childCount];
        
        for(int i = 0; i < childCount; i++) {
            Transform child = trans.GetChild(i);
            items[i] = child.GetComponentInChildren<UISprite>();
            startingScale[i] = child.localScale;
        }
    }
    
    void Update() {

        if (mDrag.panel == null) return;
        
        Transform dt = mDrag.panel.cachedTransform;
        Vector3 center = dt.localPosition;

        Transform trans = transform;
        
        for(int i = 0, imax = items.Length; i < imax; i++) {
            float distance = (scrollViewIsHorizontal == true) ? Mathf.Abs(center.x + items[i].transform.parent.localPosition.x):
                                                                Mathf.Abs(center.y + items[i].transform.parent.localPosition.y);
            
            float modifierStrength = 1.0f - distance / maxOffset;
            
            if(modifierStrength > 0.0f) {
                items[i].depth = Mathf.RoundToInt(modifierStrength * 10.0f);
                items[i].transform.parent.localScale = new Vector3(startingScale[i].x + scaleIncreaseBy.x * modifierStrength,
                                                                   startingScale[i].y + scaleIncreaseBy.y * modifierStrength,
                                                                   startingScale[i].z + scaleIncreaseBy.z * modifierStrength);
            } else {
                items[i].depth = 0;
                                items[i].transform.parent.localScale = new Vector3(startingScale[i].x, startingScale[i].y, startingScale[i].z);
            }
        }
    }
}