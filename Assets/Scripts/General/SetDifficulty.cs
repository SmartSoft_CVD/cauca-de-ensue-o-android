﻿using UnityEngine;
using System.Collections;

public class SetDifficulty : MonoBehaviour
{
	public GameObject [] buttonsDifficulty;
	public UISprite spriteImageSelected;
	private bool loading = false;
	// Use this for initialization
	void Start ()
	{
		loading = false;
		foreach (GameObject botonActual in buttonsDifficulty) {
				UIEventListener.Get (botonActual).onClick += SetLvl;
		}

		spriteImageSelected.spriteName = PlayerPrefs.GetString("ImagenRompecabezas");
	}

	void SetLvl (GameObject gO)
	{
		switch (gO.name) {
		case "ButtonFacil":
				PlayerPrefs.SetString ("Difficulty", "facil");
			
				break;
			
		case "ButtonMedio":
				PlayerPrefs.SetString ("Difficulty", "medio");
			
				break;
			
		case "ButtonDificil":
				PlayerPrefs.SetString ("Difficulty", "dificil");
			
				break;
		default:
				break;
		}

		loading = true;
		PlayerPrefs.SetString("SetPuzzleImage", "true");


	
	}

	void Update()
	{
//		if(loading)
//		{
//			loading = false;
//			if(Hud.hudPUP_Parent == null)
//			{
//				Hud.hudpartsOfTutorial = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Loading"));
//				Hud.hudpartsOfTutorial.name = "Cargando";				
//				Hud.hudpartsOfTutorial.transform.parent = Hud.hudMainParent.transform;				
//				Hud.hudpartsOfTutorial.transform.localPosition = Vector3.zero;
//				Hud.hudpartsOfTutorial.transform.localRotation = Quaternion.identity;
//				Hud.hudpartsOfTutorial.transform.localScale = Vector3.one;
//			}
//
//		}
	}
}
