﻿using UnityEngine;

/// <summary>
/// Tween the sprte's fillamount.
/// </summary>
[ExecuteInEditMode]
[AddComponentMenu("NGUI/Tween/Tween Fill")]
public class TweenFill : UITweener
{
	public float from = 1;
	public float to = 1;
	public bool updateTable = false;
	
	static UISprite mSprite;
	UITable mTable;

	public float fillAmount { get { return mSprite.fillAmount; } set { mSprite.fillAmount = value; } }

	protected override void OnUpdate (float factor, bool isFinished)
	{
		mSprite.fillAmount = from * (1f - factor) + to * factor;

		if (updateTable)
		{
			if (mTable == null)
			{
				mTable = NGUITools.FindInParents<UITable>(gameObject);
				if (mTable == null) { updateTable = false; return; }
			}
			mTable.repositionNow = true;
		}
	}
	
	void Awake ()
	{
		mSprite = GetComponentInChildren<UISprite>();
	}

	/// <summary>
	/// Start the tweening operation.
	/// </summary>

	static public TweenFill Begin (GameObject go, float duration, float fillAmount)
	{
		TweenFill comp = UITweener.Begin<TweenFill>(go, duration);
		comp.from = comp.fillAmount;
		comp.to = fillAmount;

		if (duration <= 0f)
		{
			comp.Sample(1f, true);
			comp.enabled = false;
		}
		return comp;
	}
}
