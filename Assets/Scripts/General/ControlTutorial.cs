﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ControlTutorial : MonoBehaviour
{
	public GameObject containerButtons;
	public UIScrollView scrollView;
	public UICenterOnChild grid;
	
	public GameObject objCentered;
	
	public UIButton buttonRight;
	public UIButton buttonLeft;

	[SerializeField]
	private List <GameObject> partsOfTutorial;

	public TweenColor tweenColorBackground;
	public TweenPosition tweenPositionTutorial;

	public TweenPosition tweenPositionCredits;
	public BoxCollider colliderBackgroundTutorial;

	public UIButton buttonOpenTutorial;
	public UIButton buttonCloseTutorial;

	public UIButton buttonOpenCredtis;


	private bool openendCredits = false;


	public UIButton buttonCollider;
	public UIButton buttonSound;

	public bool mute;

	#region Métodos Créditos
	void OpenPanelCredits()
	{
		buttonCollider.GetComponent<TweenPosition>().PlayForward();
		openendCredits = true;
		tweenColorBackground.PlayForward();
		tweenPositionCredits.PlayForward();
		colliderBackgroundTutorial.enabled = true;
	}

	void ClosePanelCredits()
	{
		openendCredits = false;
		buttonCollider.GetComponent<TweenPosition>().PlayReverse();
		tweenPositionCredits.enabled = false;
		tweenColorBackground.PlayReverse();
		tweenPositionCredits.ResetToBeginning();
		colliderBackgroundTutorial.enabled = false;
	}
	#endregion
	void OpenPanelTutorial()
	{
		tweenColorBackground.PlayForward();
		tweenPositionTutorial.PlayForward();
		colliderBackgroundTutorial.enabled = true;
	}

	void ClosePanelTutorial()
	{
		tweenColorBackground.PlayReverse();
		tweenPositionTutorial.PlayReverse();
		colliderBackgroundTutorial.enabled = false;
	}


	// Use this for initialization
	void Start ()
	{
		if(PlayerPrefs.GetString("Mute") == "false")
		{
			mute = false;
			buttonSound.gameObject.GetComponentInChildren<UISprite>().spriteName = "btn_sound_on";
		}
		else
		{
			buttonSound.gameObject.GetComponentInChildren<UISprite>().spriteName = "btn_sound_off";
			mute = true;
		}

		EventDelegate.Add(buttonSound.onClick, delegate(){
			
			mute = !mute;
		});
		EventDelegate.Add(buttonCollider.onClick, delegate(){

			ClosePanelCredits();
		});


		EventDelegate.Add(buttonOpenTutorial.onClick, delegate(){
			OpenPanelTutorial();
		});

		EventDelegate.Add(buttonCloseTutorial.onClick, delegate(){
			ClosePanelTutorial();
		});


		EventDelegate.Add(buttonOpenCredtis.onClick, delegate(){
			OpenPanelCredits();
		});


		foreach (Transform currentButtonEthnicity in containerButtons.transform)
		{
			partsOfTutorial.Add(currentButtonEthnicity.gameObject);
		}

		IComparer c = new CaseInsensitiveComparer();
		
		for(int i = 0 ; i < partsOfTutorial.Count; i ++){
			for(int j = i+1 ; j < partsOfTutorial.Count; j++){
				if(c.Compare(partsOfTutorial[j].name , partsOfTutorial[i].name) == -1){
					GameObject temp = partsOfTutorial[i];
					partsOfTutorial[i] = partsOfTutorial[j];
					partsOfTutorial[j] = temp;
				}
			}
		}


		NGUITools.SetActive (buttonLeft.gameObject, false);

		EventDelegate.Add(buttonRight.onClick, delegate(){		
			AdvanceByButtonsRight(); });
		
		EventDelegate.Add(buttonLeft.onClick, delegate(){
			AdvanceByButtonsLeft();
		});

		grid.onFinished += MetodoOnDrag;
	
	}
	
	// Update is called once per frame
	void Update ()
	{
		objCentered = grid.centeredObject;

		if(Input.GetKeyDown(KeyCode.Escape) && openendCredits)
		{
			buttonCollider.SendMessage("OnClick");
		}
		else if(Input.GetKeyDown(KeyCode.Escape) && !openendCredits)
		{
			Debug.Log("Salir de la app");
			Application.Quit();
		}

		if(mute)
		{
			PlayerPrefs.SetString("Mute", "true");
			buttonSound.gameObject.GetComponentInChildren<UISprite>().spriteName = "btn_sound_off";
		}
		else
		{
			buttonSound.gameObject.GetComponentInChildren<UISprite>().spriteName = "btn_sound_on";
			PlayerPrefs.SetString("Mute", "false");
		}
	
	}

	#region Métodos
	void AdvanceByButtonsRight()
	{
		Debug.Log("Se presiono boton derecho");
		
		
		
		for (int n = 0; n < partsOfTutorial.Count; n++)
		{
			
			
			
			if(objCentered.Equals(partsOfTutorial[n]) && !objCentered.Equals(partsOfTutorial[partsOfTutorial.Count - 1]) )
			{
				
				partsOfTutorial[n + 1].SendMessage("OnClick");
				
				
				
				
			}
		}
	}
	
	void AdvanceByButtonsLeft()
	{
		Debug.Log("Se presiono boton izquierdo");
		for (int n = 0; n < partsOfTutorial.Count; n++)
		{
			if(objCentered.Equals(partsOfTutorial[n]) && !objCentered.Equals(partsOfTutorial[0]) )
			{
				partsOfTutorial[n - 1].SendMessage("OnClick");
				
			}
		}
	}
	#endregion

	void MetodoOnDrag()
	{		
		if(objCentered.Equals(partsOfTutorial[0]))
		{
			NGUITools.SetActive(buttonRight.gameObject, true);
			NGUITools.SetActive(buttonLeft.gameObject, false);
		}
		else if(objCentered.Equals(partsOfTutorial[partsOfTutorial.Count - 1]))
		{
			NGUITools.SetActive(buttonRight.gameObject, false);
			NGUITools.SetActive(buttonLeft.gameObject, true);
		}
		else
		{
			NGUITools.SetActive(buttonLeft.gameObject, true);
			NGUITools.SetActive(buttonRight.gameObject, true);
		}
		
		
	}
}
