﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SelectionEthnicity : MonoBehaviour
{
	public GameObject containerButtons;
	public string nameEthnicity;
	public UIScrollView scrollView;
	public UICenterOnChild grid;

	public GameObject objCentered;

	public UIButton buttonRight;
	public UIButton buttonLeft;
	public string valor;
	public bool valorBooleano;

	[SerializeField]
	private List <GameObject> buttonsEthnicity;

	private AudioSource audioSource;

	public AudioClip [] ethnicityClips;

	void Start()
	{
		audioSource = this.GetComponent<AudioSource>();
		buttonsEthnicity.Sort();

		foreach (Transform currentButtonEthnicity in containerButtons.transform)
		{
			buttonsEthnicity.Add(currentButtonEthnicity.gameObject);
		}

		for (int g = 0; g < buttonsEthnicity.Count; g++)
		{
			NavigationMap.ChangeOfScreen(buttonsEthnicity[g], eScreen.LEVELS);
		}

		foreach (GameObject buttonOfList in buttonsEthnicity)
		{
			UIEventListener.Get(buttonOfList).onClick += delegate (GameObject gO) {

				nameEthnicity = gO.name;

				string nameComplete;
				string [] partsOfName;
				
				nameComplete = nameEthnicity;
				partsOfName = nameComplete.Split(new char[]{ '_' });



				if(nameEthnicity != "")
					PlayerPrefs.SetString("Ethnicity", partsOfName[1]);








			};
		}




		NGUITools.SetActive (buttonLeft.gameObject, false);
		buttonsEthnicity[1].GetComponent<UIButton>().isEnabled = false;
		buttonsEthnicity[2].GetComponent<UIButton>().isEnabled = false;
		buttonsEthnicity[3].GetComponent<UIButton>().isEnabled = false;
		buttonsEthnicity[4].GetComponent<UIButton>().isEnabled = false;		
		buttonsEthnicity[1].GetComponent<UIPlayTween>().enabled = false;
		buttonsEthnicity[2].GetComponent<UIPlayTween>().enabled = false;
		buttonsEthnicity[3].GetComponent<UIPlayTween>().enabled = false;
		buttonsEthnicity[4].GetComponent<UIPlayTween>().enabled = false;






	

		grid.CenterOn(buttonsEthnicity[0].transform);


		#region
		EventDelegate.Add(buttonRight.onClick, delegate(){		
//			buttonsEthnicity[1].SendMessage("OnClick");
			AdvanceByButtonsRight(); });
		
		EventDelegate.Add(buttonLeft.onClick, delegate(){
			AdvanceByButtonsLeft();
		});
		#endregion


		grid.onFinished += MetodoOnDrag;


//		if(objCentered != null)
//		{
//			switch (objCentered.name)
//			{
//			case "1_Afro":
//				audio.PlayOneShot(ethnicityClips[0]);
//				break;
//				
//			case "2_Campesinos":
//				audio.PlayOneShot(ethnicityClips[1]);
//				break;
//				
//			case "3_Guambianos":
//				audio.PlayOneShot(ethnicityClips[2]);
//				break;
//				
//			case "4_Nasa":
//				audio.PlayOneShot(ethnicityClips[3]);
//				break;
//				
//			case "5_Yanaconas":
//				audio.PlayOneShot(ethnicityClips[4]);
//				break;
//			default:
//				break;
//			}
//		}
	}

	void Update()
	{
		objCentered = grid.centeredObject;

		//Lógica necesaria para controlar los distintos clips





	}



	#region Métodos
	void AdvanceByButtonsRight()
	{
		Debug.Log("Se presiono boton derecho");


		
		for (int n = 0; n < buttonsEthnicity.Count; n++)
		{



			if(objCentered.Equals(buttonsEthnicity[n]) && !objCentered.Equals(buttonsEthnicity[buttonsEthnicity.Count - 1]))
			{

				buttonsEthnicity[n + 1].SendMessage("OnClick");

				Debug.Log("Este es el actual elemento de la lista: " + buttonsEthnicity[n].ToString());



				
			}
		}
	}
	
	void AdvanceByButtonsLeft()
	{
		Debug.Log("Se presiono boton izquierdo");
		for (int n = 0; n < buttonsEthnicity.Count; n++)
		{
			if(objCentered.Equals(buttonsEthnicity[n]) && !objCentered.Equals(buttonsEthnicity[0]))
			{
				buttonsEthnicity[n - 1].SendMessage("OnClick");
				
			}
		}
	}
	#endregion
	void MetodoOnDrag()
	{
		if(objCentered != null)
		{
			switch (objCentered.name)
			{
			case "1_Afro":
				audio.Stop();
				audio.PlayOneShot(ethnicityClips[0]);
				audio.loop = true;
				break;
				
			case "2_Campesinos":
				audio.Stop();
				audio.PlayOneShot(ethnicityClips[1]);
				audio.loop = true;
				break;
				
			case "3_Guambianos":
				audio.Stop();
				audio.PlayOneShot(ethnicityClips[2]);
				audio.loop = true;
				break;
				
			case "4_Nasa":
				audio.Stop();
				audio.PlayOneShot(ethnicityClips[3]);
				audio.loop = true;
				break;
				
			case "5_Yanaconas":
				audio.Stop();
				audio.PlayOneShot(ethnicityClips[4]);
				audio.loop = true;
				break;
			default:
				break;
			}
		}

		//objCentered = grid.centeredObject;
		
		for (int d = 0; d < buttonsEthnicity.Count; d++)
		{
			if(objCentered.Equals(buttonsEthnicity[d]))
			{

				//buttonsEthnicity[d].transform.localScale = new Vector3(1.2f, 1.2f, 1.2f);
				buttonsEthnicity[d].GetComponent<UIButton>().isEnabled = true;
				buttonsEthnicity[d].GetComponent<UIPlayTween>().enabled = true;
				buttonsEthnicity[d].transform.FindChild("Clouds").GetComponent<TweenScale>().PlayForward();
				buttonsEthnicity[d].transform.FindChild("Background").GetComponent<Spin>().enabled = true;

				buttonsEthnicity[d].transform.FindChild("Name").GetComponent<TweenPosition>().PlayForward();


			}
			else
			{
				//buttonsEthnicity[d].transform.localScale = Vector3.one;
				buttonsEthnicity[d].GetComponent<UIButton>().isEnabled = false;
				buttonsEthnicity[d].GetComponent<UIPlayTween>().enabled = false;
				buttonsEthnicity[d].transform.FindChild("Clouds").GetComponent<TweenScale>().PlayReverse();
				buttonsEthnicity[d].transform.FindChild("Background").transform.localRotation = Quaternion.identity;
				buttonsEthnicity[d].transform.FindChild("Background").GetComponent<Spin>().enabled = false;

				buttonsEthnicity[d].transform.FindChild("Name").GetComponent<TweenPosition>().PlayReverse();
			}
		}
		
		
		
		if(objCentered.Equals(buttonsEthnicity[0]))
		{
			NGUITools.SetActive(buttonRight.gameObject, true);
			NGUITools.SetActive(buttonLeft.gameObject, false);
		}
		else if(objCentered.Equals(buttonsEthnicity[buttonsEthnicity.Count - 1]))
		{
			NGUITools.SetActive(buttonRight.gameObject, false);
			NGUITools.SetActive(buttonLeft.gameObject, true);
		}
		else
		{
			NGUITools.SetActive(buttonLeft.gameObject, true);
			NGUITools.SetActive(buttonRight.gameObject, true);
		}


	}
}
