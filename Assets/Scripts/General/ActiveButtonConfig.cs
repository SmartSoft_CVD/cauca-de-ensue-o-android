﻿using UnityEngine;
using System.Collections;

public class ActiveButtonConfig : MonoBehaviour {

	public GameObject buttonConfig;


	void Awake()
	{
		Time.timeScale = 1.0f;
		PlayerPrefs.SetString("Mute", "false");
	}
	// Use this for initialization
	void Start ()
	{
		buttonConfig.SendMessage("OnClick");
		StartCoroutine(ActiveConfig(2.0f));
	
	}

	IEnumerator ActiveConfig(float timeToHide)
	{
		yield return new WaitForSeconds(timeToHide);
		buttonConfig.SendMessage("OnClick");
	}

	// Update is called once per frame
	void Update () {
	
	}
}
