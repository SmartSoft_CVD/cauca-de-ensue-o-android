﻿using UnityEngine;
using System.Collections;

public class LoadScene : MonoBehaviour {

	bool launchingGame = false;
	public GameObject progressBarNGUI;
	public int progreso;

	AsyncOperation async;

	IEnumerator NewGame()
	{

		
		while (!async.isDone)
		{
			float p = async.progress *100f;
			int pRounded = Mathf.RoundToInt(p);
			progreso = (int)pRounded;
			
			yield return true;
		}
	}
	
	
	void Start()
	{
		//StartCoroutine("launchLevel");

		InvokeRepeating("launchLevel",0.5f,0.005f);
	}

	void Update()
	{
		//ProgressMethod();
	}

	void ProgressMethod()
	{
		//progressBarNGUI.value = (float)progreso;
	}

	void LoadNewScene ()
	{
		if(async != null){
			async.allowSceneActivation = true;
		}
	}

	public void launchLevel()
	{
//		if (launchingGame == false) {
//			launchingGame=true;
//			
//			AsyncOperation async = Application.LoadLevelAsync ("Test");
//			Debug.Log ("Loading asynch waiting yield");
//			
//			Debug.Log ("async = " + async + " async.progress = " + async.progress);
//			while(async.isDone == false) {
//				float p = async.progress *100f;
//				int pRounded = Mathf.RoundToInt(p);
//				string perc = pRounded.ToString();
//				
//				Debug.Log (" async.progress = " + async.progress + ", i guess this will show always 0 in the console");
//				Debug.Log ("but this should do: loading level:" + perc + " %.");
//				progressBarNGUI.value = (float)pRounded;
//				
//				yield return true;
//			}
//			Debug.Log ("but this should do: loading done: 100%.");
//		}

		if(async == null)
		{
			async = Application.LoadLevelAsync(1);
			async.allowSceneActivation = false;
		}
		
		progressBarNGUI.GetComponent<UISlider>().value = async.progress;
		
		if(async.isDone || async.progress >= 0.9f)
		{
			Debug.Log("Time to show the button");
			LoadNewScene();
		}

			CancelInvoke("letsLoad");

	}


}
