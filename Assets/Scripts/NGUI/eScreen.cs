﻿using UnityEngine;
using System.Collections;

public enum eScreen
{
	/* Este enum almacenara las diferentes pantallas del juego
	 * NOTA: una pantalla puede estar conformada por varios prefabs */
	
	NONE,
	// Logo
	LOGO,

	ETHNICITY,

	LEVELS,

	SETTING_LEVEL, 

	PUZZLE

	/* En caso de que existan mas pantallas en el juego se
	 * deberán ir agregando como parte de las constantes de este enum */
}
