﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class NavigationMap : MonoBehaviour
{
	void Start()
	{
		Hud.switchToState(eScreen.LOGO);


	}

	public void Update()
	{


	}
	
	/// <summary>
	/// Cambia la interfaz por la de la siguiente pantalla
	/// </summary>
	/// <param name="gO"> botón que permite hacer la transición a la siguiente pantalla </param>
	/// <param name="screenToLoad"> La pantalla que deseamos cargar.</param>
	public static void ChangeOfScreen(GameObject gO, eScreen screenToLoad)
	{
		// -----------------------------------------------> Paso 1 ----------------------------------------------->
		if(gO.GetComponent<UIButton>() != null)
		{
			// -----------------------------------------------> Paso 2 ----------------------------------------------->
			UIButton compButton = gO.GetComponent<UIButton>();

			// -----------------------------------------------> Paso 3 ----------------------------------------------->
			EventDelegate.Add(compButton.onClick, delegate()
          	{
				// -----------------------------------------------> Paso 4 ----------------------------------------------->
				UIPlayTween playTweens = gO.GetComponent<UIPlayTween>();

				// -----------------------------------------------> Paso 5 ----------------------------------------------->
				if(playTweens == null)
				{
					// -----------------------------------------------> Paso 6 ----------------------------------------------->
					Debug.LogError("El objeto "  + gO.name + " no contiene un componente UIPlayTween");
				}
				else
				{
					// -----------------------------------------------> Paso 7 ----------------------------------------------->
					playTweens.tweenTarget = GameObject.Find(Hud.rootHUD);

					// -----------------------------------------------> Paso 8 ----------------------------------------------->
					EventDelegate.Add(playTweens.onFinished, delegate()
                  	{
						// -----------------------------------------------> Paso 9 ----------------------------------------------->
						Hud.switchToState(screenToLoad);
					});
				}
				
			});
		}
	}
}
