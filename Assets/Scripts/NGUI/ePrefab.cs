﻿using UnityEngine;
using System.Collections;

public enum ePrefab
{
	/* Este enum almacenara los diferentes prefabs que conforman las pantallas del juego
	 * NOTA: un prefab puede pertenecer a una o varias pantallas */
	
	/* Para diferenciar las Pantallas de los Prefabs usaremos como notación:
	 * _PREFAB al finalizar el nombre de cada una de las constantes de este enum */
	
	NONE,
	
	// Pantalla "Logo"
	LOGO_PREFAB,

	// Pantalla  "Selección Étnia"
	ETHNICITY_PREFAB,

	// Pantalla "Niveles"
	LEVELS_PREFAB,

	// Pantalla "Configurar nivel"
	SETTING_LEVEL_PREFAB,

	// Pantalla "Rompecabezas"
	PUZZLE_PREFAB
	
	/* En caso de que existan mas Prefabs en el juego se
	 * deberán ir agregando como parte de las constantes de este enum */
}
