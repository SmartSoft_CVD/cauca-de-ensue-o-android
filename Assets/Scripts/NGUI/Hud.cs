﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Hud : MonoBehaviour
{
	#region Variables
	// Ruta del objeto "HUD" en la jerarquia 
	public static string rootHUD = "UI Root/Camera/HUD";
	// Referencia al objeto "HUD" en la jerarquia
	public static GameObject hudMainParent = GameObject.Find (rootHUD);
	
	private static bool guardarAvatar = false;
	
	// Listas de Prefab 
	private static List<ePrefab> requestedPrefabs = new List<ePrefab> ();
	private static List<ePrefab> loadedPrefabs = new List<ePrefab> ();
	
	// Estados
	private static eScreen hudCurrentState = eScreen.NONE;
	private static bool isSwitchingState = false;


	// Referencias a los Prefabs (Parents)
	
	// - LG: Logo
	private static GameObject hudLG_Parent;

	// - ET: Étnia
	private static GameObject hudET_Parent;

	// - LE: Niveles
	public static GameObject hudLE_Parent;

	// - SL: Niveles
	public static GameObject hudSL_Parent;

	// - PU: Rompecabezas
	public static GameObject hudPU_Parent;

	// - PUP: Rompecabezas Plugin
	public static GameObject hudPUP_Parent;


	public static GameObject hudpartsOfTutorial;
	public static string rootpartsOfTutorial = "Prefabs/partsOfTutorial";

	#endregion

	#region ->  switchToState()  <-
	public static void switchToState (eScreen newState)
	{


		// Si el nuevo estado es diferente al actual y no se esta ya en un proceso de cambio de HUD...
		if ((newState != hudCurrentState) && (isSwitchingState == false))
		{



			
			
			// Activamos la bandera que indica el inicio de un proceso de cambio de HUD
			isSwitchingState = true;
			
			// Reiniciamos la lista de prefabs a cargar
			requestedPrefabs.Clear ();
			
			// Asignamos el nuevo estado
			hudCurrentState = newState;
			
			// Agregamos a las listas respectivas los HUDs que deseamos activar y que pertenecen solo al nuevo HUD State
			switch (hudCurrentState)
			{
				case eScreen.LOGO:
					requestedPrefabs.Add (ePrefab.LOGO_PREFAB);

					break;

				case eScreen.ETHNICITY:
					requestedPrefabs.Add (ePrefab.ETHNICITY_PREFAB);
					break;

				case eScreen.LEVELS:
					requestedPrefabs.Add (ePrefab.LEVELS_PREFAB);
					break;

				case eScreen.SETTING_LEVEL:
					requestedPrefabs.Add (ePrefab.SETTING_LEVEL_PREFAB);
					break;

				case eScreen.PUZZLE:
					requestedPrefabs.Add (ePrefab.PUZZLE_PREFAB);
					break;
			}
			
			//OJO NO TOCAR// - Determinamos cuales de los prefabs a cargar no deben hacerlo, por ya estar cargados:
			List<ePrefab> prefabsToCreate = new List<ePrefab> ();
			
			// Recorremos la lista de prefabs a cargar...
			for (int currentRequestedPrefab = 0; currentRequestedPrefab < requestedPrefabs.Count; currentRequestedPrefab++)
			{
				bool currentRequestedPrefabExist = false;
				
				// Recorremos la lista de prefabs cargados actualmente...
				for (int currentLoadedPrefab = 0; currentLoadedPrefab < loadedPrefabs.Count; currentLoadedPrefab++)
				{
					// Realizamos la verificacion
					if (requestedPrefabs [currentRequestedPrefab] == loadedPrefabs [currentLoadedPrefab])
					{
						// El prefab actual no debe ser creado, ya que esta siendo solicitado nuevamente
						currentRequestedPrefabExist = true;
						
						// Salimos del ciclo inmediatamente...
						break;
					}
				}
				
				// Si el prefab actual requerido no existe...
				if (currentRequestedPrefabExist == false)
				{
					// Es necesaria su creacion...
					prefabsToCreate.Add (requestedPrefabs [currentRequestedPrefab]);
				}
			}
			
			// - Ahora determinamos cuales de los prefabs actualmente cargados debemos eliminar:
			List<ePrefab> prefabsToDelete = new List<ePrefab> ();
			
			// Recorremos la lista de prefabs cargados actualmente...
			for (int currentLoadedPrefab = 0; currentLoadedPrefab < loadedPrefabs.Count; currentLoadedPrefab++)
			{
				bool currentLoadedPrefabExist = false;
				
				// Recorremos la lista de prefabs a cargar...
				for (int currentRequestedPrefab = 0; currentRequestedPrefab < requestedPrefabs.Count; currentRequestedPrefab++)
				{
					// Realizamos la verificacion
					if (requestedPrefabs [currentRequestedPrefab] == loadedPrefabs [currentLoadedPrefab])
					{
						// El prefab actual no debe ser eliminado, ya que esta siendo solicitado nuevamente
						currentLoadedPrefabExist = true;
						
						// Salimos del ciclo inmediatamente...
						break;
					}
				}
				
				// Si el prefab actual cargado no es requerido...
				if (currentLoadedPrefabExist == false)
				{
					// Es necesaria su eliminacion...
					prefabsToDelete.Add (loadedPrefabs [currentLoadedPrefab]);
				}
			}
			
			// Eliminamos los prefabs que no son solicitados...
			for (int index = 0; index < prefabsToDelete.Count; index ++)
			{
				deletePrefab (prefabsToDelete [index]);
			}
			
			// Creamos los prefabs que faltan...
			for (int index = 0; index < prefabsToCreate.Count; index ++)
			{
				loadPrefab (prefabsToCreate [index]);
			}
			
			// Indicamos que el cambio de HUD ha finalizado
			isSwitchingState = false;




		}
		//FIN NO TOCAR


	}
	#endregion

	#region ->  Carga de prefabs  <-    
	private static void loadPrefab (ePrefab prefab)
	{		
		// Agregamos el prefab a la lista...
		loadedPrefabs.Add (prefab);	
		
		switch (prefab)
		{			
			#region ->  eHudPrefab.LOGO_PREFAB  <-
			//Paso 4 crear mi case para cada prefab de acuerdo al ejemplo
		case ePrefab.LOGO_PREFAB:

			#region -> Eliminar Puzzle
			if (hudPUP_Parent != null) {
				
				// Desactivamos la pantalla actual
				hudPUP_Parent.SetActive (false); // (opcional)
				
				// Eliminamos HUD padre
				GameObject.Destroy (hudPUP_Parent);
				hudPUP_Parent = null;
			}
			#endregion


				
			// Cargamos el Prafab correspondiente
			Hud.hudPUP_Parent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Puzzle/PuzzleToResolved"));			
			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			Hud.hudPUP_Parent.name = "Puzzle To Resolved";			
			// Lo agregamos como hijo del HUD
			//			hudPUP_Parent.transform.parent = hudMainParent.transform;			
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			Hud.hudPUP_Parent.transform.localPosition = Vector3.zero;
			Hud.hudPUP_Parent.transform.localRotation = Quaternion.identity;
			Hud.hudPUP_Parent.transform.localScale = Vector3.one;
		

		
			
			// Cargamos el Prafab correspondiente
			hudLG_Parent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Logo"));
			
			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			hudLG_Parent.name = "Logo";
			
			
			// Lo agregamos como hijo del HUD
			hudLG_Parent.transform.parent = hudMainParent.transform;
			
			
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			hudLG_Parent.transform.localPosition = Vector3.zero;
			hudLG_Parent.transform.localRotation = Quaternion.identity;
			hudLG_Parent.transform.localScale = Vector3.one;
			
			
			NavigationMap.ChangeOfScreen(hudLG_Parent.GetComponent<Widgets>().widgetsList[0], eScreen.ETHNICITY);
			break;
			#endregion

			#region ->  eHudPrefab.ETHNICITY_PREFAB  <-
			//Paso 4 crear mi case para cada prefab de acuerdo al ejemplo
		case ePrefab.ETHNICITY_PREFAB:

//			#region -> Eliminar Puzzle
//			if (hudPUP_Parent != null) {
//				
//				// Desactivamos la pantalla actual
//				hudPUP_Parent.SetActive (false); // (opcional)
//				
//				// Eliminamos HUD padre
//				GameObject.Destroy (hudPUP_Parent);
//				hudPUP_Parent = null;
//			}
//			#endregion
			
			// Cargamos el Prafab correspondiente
			hudET_Parent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/SelectionEthnicity"));
			
			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			hudET_Parent.name = "Selection Ethnicity";
			
			
			// Lo agregamos como hijo del HUD
			hudET_Parent.transform.parent = hudMainParent.transform;
			
			
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			hudET_Parent.transform.localPosition = Vector3.zero;
			hudET_Parent.transform.localRotation = Quaternion.identity;
			hudET_Parent.transform.localScale = Vector3.one;
			
			
		 	NavigationMap.ChangeOfScreen(hudET_Parent.GetComponent<Widgets>().widgetsList[0], eScreen.LOGO);
			break;
			#endregion			

			#region ->  eHudPrefab.LEVELS_PREFAB  <-
			//Paso 4 crear mi case para cada prefab de acuerdo al ejemplo
		case ePrefab.LEVELS_PREFAB:

//			#region -> Eliminar Puzzle
//			if (hudPUP_Parent != null) {
//				
//				// Desactivamos la pantalla actual
//				hudPUP_Parent.SetActive (false); // (opcional)
//				
//				// Eliminamos HUD padre
//				GameObject.Destroy (hudPUP_Parent);
//				hudPUP_Parent = null;
//			}
//			#endregion

			#region -> Eliminar Puzzle
			if (hudPUP_Parent != null) {
				
				// Desactivamos la pantalla actual
				hudPUP_Parent.SetActive (false); // (opcional)
				
				// Eliminamos HUD padre
				GameObject.Destroy (hudPUP_Parent);
				hudPUP_Parent = null;
			}
			#endregion
			
			
			
			// Cargamos el Prafab correspondiente
			Hud.hudPUP_Parent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Puzzle/PuzzleToResolved"));			
			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			Hud.hudPUP_Parent.name = "Puzzle To Resolved";			
			// Lo agregamos como hijo del HUD
			//			hudPUP_Parent.transform.parent = hudMainParent.transform;			
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			Hud.hudPUP_Parent.transform.localPosition = Vector3.zero;
			Hud.hudPUP_Parent.transform.localRotation = Quaternion.identity;
			Hud.hudPUP_Parent.transform.localScale = Vector3.one;
			
			// Cargamos el Prafab correspondiente
			hudLE_Parent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Levels"));
			
			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			hudLE_Parent.name = "Levels";
			
			
			// Lo agregamos como hijo del HUD
			hudLE_Parent.transform.parent = hudMainParent.transform;
			
			
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			hudLE_Parent.transform.localPosition = Vector3.zero;
			hudLE_Parent.transform.localRotation = Quaternion.identity;
			hudLE_Parent.transform.localScale = Vector3.one;

			NavigationMap.ChangeOfScreen(hudLE_Parent.GetComponent<Widgets>().widgetsList[0], eScreen.ETHNICITY);
			
			break;
			#endregion			

			#region ->  eHudPrefab.SETTING_LEVEL_PREFAB  <-
			//Paso 4 crear mi case para cada prefab de acuerdo al ejemplo
		case ePrefab.SETTING_LEVEL_PREFAB:

//			#region -> Eliminar Puzzle
//			if (hudPUP_Parent != null) {
//				
//				// Desactivamos la pantalla actual
//				hudPUP_Parent.SetActive (false); // (opcional)
//				
//				// Eliminamos HUD padre
//				GameObject.Destroy (hudPUP_Parent);
//				hudPUP_Parent = null;
//			}
//			#endregion

			// Cargamos el Prafab correspondiente
			hudSL_Parent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/SettingLevel"));
			
			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			hudSL_Parent.name = "Setting Level";
			
			
			// Lo agregamos como hijo del HUD
			hudSL_Parent.transform.parent = hudMainParent.transform;
			
			
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			hudSL_Parent.transform.localPosition = Vector3.zero;
			hudSL_Parent.transform.localRotation = Quaternion.identity;
			hudSL_Parent.transform.localScale = Vector3.one;
			
			NavigationMap.ChangeOfScreen(hudSL_Parent.GetComponent<Widgets>().widgetsList[0], eScreen.PUZZLE);
			NavigationMap.ChangeOfScreen(hudSL_Parent.GetComponent<Widgets>().widgetsList[1], eScreen.PUZZLE);
			NavigationMap.ChangeOfScreen(hudSL_Parent.GetComponent<Widgets>().widgetsList[2], eScreen.PUZZLE);
			NavigationMap.ChangeOfScreen(hudSL_Parent.GetComponent<Widgets>().widgetsList[3], eScreen.LEVELS);
			
			//			NavigationMap.ChangeOfScreen(hudLG_Parent.GetComponent<Widgets>().widgetsList[0], eScreen.SELECTION);
			break;
			#endregion

			#region ->  eHudPrefab.PUZZLE_PREFAB  <-
			//Paso 4 crear mi case para cada prefab de acuerdo al ejemplo
		case ePrefab.PUZZLE_PREFAB:

//			#region -> Eliminar Puzzle
//			if (hudPUP_Parent != null) {
//				
//				// Desactivamos la pantalla actual
//				hudPUP_Parent.SetActive (false); // (opcional)
//				
//				// Eliminamos HUD padre
//				GameObject.Destroy (hudPUP_Parent);
//				hudPUP_Parent = null;
//			}
//			#endregion


			
			// Cargamos el Prafab correspondiente
			hudPU_Parent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/PuzzleController"));			
			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			hudPU_Parent.name = "Puzzle Controller";			
			// Lo agregamos como hijo del HUD
			hudPU_Parent.transform.parent = hudMainParent.transform;			
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			hudPU_Parent.transform.localPosition = Vector3.zero;
			hudPU_Parent.transform.localRotation = Quaternion.identity;
			hudPU_Parent.transform.localScale = Vector3.one;
			

//			// Cargamos el Prafab correspondiente
//			hudPUP_Parent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Puzzle/PuzzleToResolved"));			
//			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
//			hudPUP_Parent.name = "Puzzle To Resolved";			
//			// Lo agregamos como hijo del HUD
////			hudPUP_Parent.transform.parent = hudMainParent.transform;			
//			// La asignaciom modifica el Transform, asi que lo reiniciamos
//			hudPUP_Parent.transform.localPosition = Vector3.zero;
//			hudPUP_Parent.transform.localRotation = Quaternion.identity;
//			hudPUP_Parent.transform.localScale = Vector3.one;
//
//			if(hudPUP_Parent != null)
//			{
//				if (hudpartsOfTutorial != null) {
//					
//					// Desactivamos la pantalla actual
//					hudpartsOfTutorial.SetActive (false); // (opcional)
//					
//					// Eliminamos HUD padre
//					GameObject.Destroy (hudpartsOfTutorial);
//					hudpartsOfTutorial = null;
//				}
//			}



			break;
			#endregion
		}
	}
	#endregion

	#region ->  Eliminacion de Prefabs de la RAM  <-    
	private static void deletePrefab (ePrefab prefab)
	{
		// Removemos el prefab a la lista...
		loadedPrefabs.Remove (prefab);
		//Paso 6 crear un case por cada prefab 
		switch (prefab) {
			
			#region ->  eHudPrefab.LOGO_PREFAB  <-
		case ePrefab.LOGO_PREFAB:
			
			// - LG: Logo
			if (hudLG_Parent != null) {
				
				// Desactivamos la pantalla actual
				hudLG_Parent.SetActive (false); // (opcional)
				
				// Eliminamos HUD padre
				GameObject.Destroy (hudLG_Parent);
				hudLG_Parent = null;
			}
			break;
			#endregion

			#region ->  eHudPrefab.ETHNICITY_PREFAB  <-
		case ePrefab.ETHNICITY_PREFAB:
			
			// - LG: Logo
			if (hudET_Parent != null) {
				
				// Desactivamos la pantalla actual
				hudET_Parent.SetActive (false); // (opcional)
				
				// Eliminamos HUD padre
				GameObject.Destroy (hudET_Parent);
				hudET_Parent = null;
			}
			break;
			#endregion

			#region ->  eHudPrefab.LEVELS_PREFAB  <-
		case ePrefab.LEVELS_PREFAB:
			
			// - LG: Logo
			if (hudLE_Parent != null) {
				
				// Desactivamos la pantalla actual
				hudLE_Parent.SetActive (false); // (opcional)
				
				// Eliminamos HUD padre
				GameObject.Destroy (hudLE_Parent);
				hudLE_Parent = null;
			}
			break;
			#endregion

			#region ->  eHudPrefab.SETTING_LEVEL_PREFAB  <-
		case ePrefab.SETTING_LEVEL_PREFAB:
			
			// - LG: Logo
			if (hudSL_Parent != null) {
				
				// Desactivamos la pantalla actual
				hudSL_Parent.SetActive (false); // (opcional)
				
				// Eliminamos HUD padre
				GameObject.Destroy (hudSL_Parent);
				hudSL_Parent = null;
			}
			break;
			#endregion

			#region ->  eHudPrefab.PUZZLE_PREFAB  <-
		case ePrefab.PUZZLE_PREFAB:
			
			// - LG: Logo
			if (hudPU_Parent != null) {
				
				// Desactivamos la pantalla actual
				hudPU_Parent.SetActive (false); // (opcional)
				
				// Eliminamos HUD padre
				GameObject.Destroy (hudPU_Parent);
				hudPU_Parent = null;
			}
			break;
			#endregion
			

		}
	}
	#endregion
}
